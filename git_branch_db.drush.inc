<?php

/**
 * @file
 * Git branch DB drush commands.
 */


/**
 * Implements hook_drush_command().
 */
function git_branch_db_drush_command() {
  $items = array();

  $items['git-branch-db'] = array(
    'description' => dt('Print git_branch_db information.'),
    'aliases'     => array('gbdb'),
  );

  $items['git-branch-db-create-copy-me'] = array(
    'description' => dt('Create the copy me database.'),
    'options' => array(
      'default' => dt('source is the default database'),
      'branch'  => dt('source is the current branch database'),
    ),
    'aliases' => array('gbdb-cm'),
  );

  $items['git-branch-db-create-branch-db'] = array(
    'description' => dt('Create the branch database.'),
    'options' => array(
      'default'  => dt('source is the default database'),
      'copy-me'  => dt('source is the copy me database'),
    ),
    'aliases' => array('gbdb-br'),
  );

  return $items;
}


/**
 * Print git_branch_db informations
 */
function drush_git_branch_db() {
  if (_git_branch_db_get_branch() === FALSE) {
    drush_print(dt("No git, or something went wrong when checking requirements
      \nLook at _git_branch_db_get_branch() in git_branch_db.module"));
  }
  else {
    drush_print(dt('Your system is compatible !'));
  }
}


/**
 * Callback drush_git_branch_db_create_copy_me()
 */
function drush_git_branch_db_create_copy_me() {
  $sources = array(
    'default' => array(
      'db'     => _git_branch_db_get_db_original_name(),
      'label'  => dt('Default'),
    ),
    'branch' => array(
      'db'     => _git_branch_db_get_db_branch_name(),
      'label'  => dt('Branch'),
    )
  );

  _drush_git_branch_db_copy_db($sources, _git_branch_db_get_db_copy_me_name(), 'Copy me');
}


/**
 * Callback drush_git_branch_db_create_branch_db()
 */
function drush_git_branch_db_create_branch_db() {
  $sources = array(
    'default' => array(
      'db'     => _git_branch_db_get_db_original_name(),
      'label'  => dt('Default'),
    ),
    'copy-me' => array(
      'db'     => _git_branch_db_get_db_copy_me_name(),
      'label'  => dt('Copy me'),
    )
  );

  _drush_git_branch_db_copy_db($sources, _git_branch_db_get_db_branch_name(), 'Branch');
}


/**
 * DB Copy function
 */
function _drush_git_branch_db_copy_db($sources, $db_dest, $to) {
  $db_source = NULL;
  $options = array();
  $nb_cmd_line_option = 0;

  // Check if the database are present and construct drush_choice options and cmd line
  foreach ($sources as $key => $source) {
    if ($source['db'] !== db_query("SHOW DATABASES LIKE '${source['db']}'")->fetchField()) {
      unset($sources[$key]);
    }
    else {
      $options[$key] = $source['label'] . ' : ' . $source['db'];
      if (drush_get_option($source['option'])) {
        $nb_cmd_line_option++;
        $db_source = $source['db'];
      }
    }
  }

  if ($nb_cmd_line_option > 1) {
    // Can't have more than one option, ie source
    drush_log(dt("You can't have more than one source"), 'error');
    return drush_user_abort();
  }

  // No cmd line options so we ask
  if (is_null($db_source) && count($sources) > 1) {
    $choice = drush_choice($options, 'Which data source do you want ?');

    if ($choice) {
      $db_source = $sources[$choice]['db'];
    }

    echo $db_source;
  }

  if (!is_null($db_source)
    && drush_confirm(dt('Do you want to copy « !db » database to « !to » ?', array('!db' => $db_source, '!to' => $db_dest)))) {
    _git_branch_db_copy_db($db_source, $db_dest);
    drush_log(dt('!to database is now set', array('!to' => $to)), 'success');
  }
  else {
    return drush_user_abort();
  }
}
